using Microsoft.AspNetCore.Mvc;

namespace Inna_Practice.Controllers
{
  public class HomeController: Controller
  {
    [HttpGet("")]
    public string Index() => "Inna_Practice Api is running";
  }
}