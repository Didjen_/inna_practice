using System;
using System.IO;
using Microsoft.AspNetCore.Mvc;

namespace Inna_Practice.Controllers
{
  [Route("test")]
  public class TestController : Controller
  {
    [HttpGet("")]
    public string Get([FromQuery] string message)
    {
      return !string.IsNullOrEmpty(message)
        ? $"You make a GET request with param 'message' = {message}"
        : "You make a GET request";
    }

    [HttpGet("{number:int}")]
    public string GetWithRouteParam(int number) => $"You make a GET request with route param 'number' =  {number}";

    [HttpPost("")]
    public string Post() => $"You make a POST request";

    [HttpPost("congratulation")]
    public string PostWithData([FromBody] PostDto dto)
    {
      if (string.IsNullOrWhiteSpace(dto.Name) )
      {
        throw new ArgumentException("Name is NULL or empty");
      }
      if (string.IsNullOrWhiteSpace(dto.CelebrationName) )
      {
        throw new ArgumentException("CelebrationName is NULL or empty");
      }
      return $"You make a POST request. Your celebration: \n Dear {dto.Name}, have a good {dto.CelebrationName}!";
    }

    [HttpPut("")]
    public string Put()
    {
      return $"You make a PUT request.";
    }

    [HttpDelete("")]
    public string Delete()
    {
      return $"You make a DELETE request.";
    }

    public class PostDto
    {
      public string Name { get; set; }
      public string CelebrationName { get; set; }
    }
  }
}